package server

import (
	"errors"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

// Location TODO: Comment
type Location struct {
	Lat      float64 `json:"latitude"`
	Lon      float64 `json:"longitude"`
	CityName string  `json:"city"`
}

// TODO: Write file with known ip -> Location mappings

func getLocation(ip string) Location {
	var loc Location
	var err error

	cutPort(&ip)
	if ip == "[" {
		err = errors.New("Local ip")
	} else {
		body := establishConnection("http://geoip.nekudo.com/api/")
		err = json.Unmarshal(body, &loc)
	}

	if err != nil {
		loc.Lat = 49.8695
		loc.Lon = 8.6507
		loc.CityName = "Darmstadt"
	}

	return loc
}

func cutPort(ip *string) {
	*ip = (*ip)[:strings.Index(*ip, ":")]
}

func establishConnection(url string) []byte {
	res, err := http.Get(url)

	if err != nil {
		log.Println(err)
		return []byte("fail")
	}

	// read body
	body, err := ioutil.ReadAll(res.Body)

	res.Body.Close()
	if err != nil {
		log.Println(err)
	}

	return body
}
